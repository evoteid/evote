package ru.analteam.evote.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by dima-pc on 30.11.2015.
 */
@Configuration
@ComponentScan({"ru.analteam.evote.service"})
public class AppConfig {
}
